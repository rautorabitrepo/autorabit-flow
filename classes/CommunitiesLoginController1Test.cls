/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class CommunitiesLoginController1Test {
    @IsTest(SeeAllData=true) 
    global static void testCommunitiesLoginController1 () {
       CommunitiesLoginController1 controller = new CommunitiesLoginController1();
       System.assertEquals(null, controller.forwardToAuthPage());       
    }    
}