@isTest
private class RestrictByName_Test{
    @isTest static void test() {
        AR_Shams_Object__c a = new AR_Shams_Object__c(Name = 'INVALIDNAME');
        Database.SaveResult result = Database.insert(a, false);
        System.assertEquals('The Name "'+a.Name+'" is not allowed for DML',result.getErrors()[0].getMessage());
    }
}