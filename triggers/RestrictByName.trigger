trigger RestrictByName on AR_Shams_Object__c(before insert, before update) {
    
    for (AR_Shams_Object__c a : Trigger.New) {
        if(a.Name == 'INVALIDNAME') {   //invalidname is invalid
            a.AddError('The Name "'+a.Name+'" is not allowed for DML');
        }

    }
}