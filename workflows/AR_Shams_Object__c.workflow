<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Shams123</fullName>
        <field>AR_Shams_Picklict__c</field>
        <literalValue>Opt 2</literalValue>
        <name>Shams123</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Shams AR</fullName>
        <actions>
            <name>Shams123</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AR_Shams_Object__c.Name</field>
            <operation>equals</operation>
            <value>Shams</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
